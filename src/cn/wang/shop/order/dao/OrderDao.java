package cn.wang.shop.order.dao;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import cn.wang.shop.order.vo.Order;
import cn.wang.shop.order.vo.OrderItem;
import cn.wang.shop.utils.PageHibernateCallback;

public class OrderDao extends HibernateDaoSupport {

	// Dao层的保存订单额操作
	public void save(Order order) {
		this.getHibernateTemplate().save(order);
	}

	// Dao层查询我的订单分页查询:统计个数
	public int findCountByUid(Integer uid,Integer state) {
		List<Long> list;
		if(state==null){
			String hql = "select count(*) from Order o where o.user.uid = ? ";
			 list = this.getHibernateTemplate().find(hql,new Object[]{uid});
		}else {
			String hql = "select count(*) from Order o where o.user.uid = ? and o.state=?";
			 list = this.getHibernateTemplate().find(hql,new Object[]{uid,state});
		}
		if (list != null && list.size() > 0) {
			return list.get(0).intValue();
		}
		return 0;
	}

	// Dao层查询我的订单分页查询:查询数据
	public List<Order> findPageByUid(Integer uid,Integer state, int begin, int limit) {
		List<Order> list;
		if(state==null){
			String hql = "from Order o where o.user.uid = ? order by o.ordertime desc";
			list = this.getHibernateTemplate().execute(
					new PageHibernateCallback<Order>(hql, new Object[]{uid},
							begin, limit));
		}else {
			String hql = "from Order o where o.user.uid = ? and o.state=? order by o.ordertime desc";
			list = this.getHibernateTemplate().execute(
					new PageHibernateCallback<Order>(hql, new Object[]{uid,state},
							begin, limit));
		}
		if (list != null && list.size() > 0) {
			return list;
		}
		return null;
	}

	// DAO层根据订单id查询订单
	public Order findByOid(Integer oid) {
		return this.getHibernateTemplate().get(Order.class, oid);
	}

	// DAO层修改订单的方法:
	public void update(Order currOrder) {
		this.getHibernateTemplate().update(currOrder);
	}

	// DAO中统计订单个数的方法
	public int findCount() {
		String hql = "select count(*) from Order";
		List<Long> list = this.getHibernateTemplate().find(hql);
		if (list != null && list.size() > 0) {
			return list.get(0).intValue();
		}
		return 0;
	}

	// DAO中分页查询订单的方法
	public List<Order> findByPage(int begin, int limit) {
		String hql = "from Order order by ordertime desc";
		List<Order> list = this.getHibernateTemplate().execute(
				new PageHibernateCallback<Order>(hql, null, begin, limit));
		return list;
	}

	// DAO中根据订单id查询订单项
	public List<OrderItem> findOrderItem(Integer oid) {
		String hql = "from OrderItem oi where oi.order.oid = ?";
		List<OrderItem> list = this.getHibernateTemplate().find(hql, oid);
		if (list != null && list.size() > 0) {
			return list;
		}
		return null;
	}
	
	//取消订单
	public String deleteByOid(Order order,Set<OrderItem> orderItem) {	
		//String hql1="delete from orderitem";
		Iterator<OrderItem> it=orderItem.iterator();
		while (it.hasNext()) {
			OrderItem orderItem2 = (OrderItem) it.next();
			this.getHibernateTemplate().delete(orderItem2);
		}
		
		//String hql2="delete from orders";
		this.getHibernateTemplate().delete(order);
		return null ;
	}

}
