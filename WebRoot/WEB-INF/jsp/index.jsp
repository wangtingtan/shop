<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags"  prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>网上商城</title>
<!--  <link href="${pageContext.request.contextPath}/css/slider.css" rel="stylesheet" type="text/css"/>-->
<link href="${pageContext.request.contextPath}/css/common.css" rel="stylesheet" type="text/css"/>
<link href="${pageContext.request.contextPath}/css/index.css" rel="stylesheet" type="text/css"/>
<link href="${pageContext.request.contextPath}/css/slide_pic.css" rel="stylesheet" type="text/css"/>

<script src="${pageContext.request.contextPath}/js/jquery-1.7.2.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.orbit.min.js"></script>
<script>
$(window).load(function() {
	$('#featured').orbit({
		bullets : true,
		startClockOnMouseOut : true,
		captionAnimation : 'slideOpen'
	});
});
</script>
</head>

<body>

<div class="container header">
	<%@ include file="header.jsp" %>
</div>	


<!-- 轮播图片 -->
<div style="width: 950px; height: 400px; margin: 0 auto;" >
	<div id="featured">
		<a class="orbit-item" href="#" data-caption="#htmlCaption1"><img src="${pageContext.request.contextPath}/images/slide_pic/img1.jpg" alt="img1"/></a>
		<a class="orbit-item" href="#" data-caption="#htmlCaption2"><img src="${pageContext.request.contextPath}/images/slide_pic/img2.jpg" alt="img1"/></a>
		<a class="orbit-item" href="#" data-caption="#htmlCaption3"><img src="${pageContext.request.contextPath}/images/slide_pic/img3.jpg" alt="img1"/></a>
		<a class="orbit-item" href="#" data-caption="#htmlCaption4"><img src="${pageContext.request.contextPath}/images/slide_pic/img4.jpg" alt="img1"/></a>
	</div>
	<span class="orbit-caption" id="htmlCaption1">花花公子秋季尚新特惠，亮瞎你的眼球</span>
	<span class="orbit-caption" id="htmlCaption2">超低价优质品牌，为你而定，不容错过</span>
	<span class="orbit-caption" id="htmlCaption3">秋冬嘉年华，改潮换代，新品爆款来袭</span>
	<span class="orbit-caption" id="htmlCaption4">最舒服睡衣，符合人体设计，秒杀全场</span>
</div>



<!-- 商品展示 -->
<div class="container index">
		<div class="span24">
			<div id="hotProduct" class="hotProduct clearfix">
					<div class="title">
						<strong>热门商品</strong>
					</div>
					<ul class="tab">
							<li class="current">
								<a href="#" target="_blank"></a>
							</li>
					</ul>
						<ul class="tabContent" style="display: block;">
							<s:iterator var="p" value="hList">
									<li>
										<a href="${ pageContext.request.contextPath }/product_findByPid.action?pid=<s:property value="#p.pid"/>" target="_blank"><img src="${pageContext.request.contextPath}/<s:property value="#p.image"/>" data-original="http://storage.shopxx.net/demo-image/3.0/201301/0ff130db-0a1b-4b8d-a918-ed9016317009-thumbnail.jpg" style="display: block;"></a>
									</li>
							</s:iterator>		
						</ul>
			</div>
		</div>
		<div class="span24">
			<div id="newProduct" class="newProduct clearfix">
					<div class="title">
						<strong>最新商品</strong>
						<a  target="_blank"></a>
					</div>
					<ul class="tab">
							<li class="current">
								<a href="#" target="_blank"></a>
							</li>
					</ul>						
						 <ul class="tabContent" style="display: block;">
						 	<s:iterator var="p" value="nList">
									<li>
										<a href="${ pageContext.request.contextPath }/product_findByPid.action?pid=<s:property value="#p.pid"/>" target="_blank"><img src="${pageContext.request.contextPath}/<s:property value="#p.image"/>" data-original="http://storage.shopxx.net/demo-image/3.0/201301/4a51167a-89d5-4710-aca2-7c76edc355b8-thumbnail.jpg" style="display: block;"></a>									</li>
									</li>
							</s:iterator>		
						</ul>			
			</div>
		</div>	
	</div>
<div class="container footer">
	<%@ include file="footer.jsp" %>
</div>
</body>
</html>